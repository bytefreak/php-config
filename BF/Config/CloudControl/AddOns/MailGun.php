<?php
namespace BF\Config\CloudControl\AddOns;

class MailGun extends \BF\Config\Configuration
{
	const KEY_SERVER = "MAILGUN_SMTP_SERVER";
	const KEY_PORT = "MAILGUN_SMTP_PORT";
	const KEY_APIKEY = "MAILGUN_API_KEY";
	const KEY_USERNAME = "MAILGUN_SMTP_LOGIN";
	const KEY_PASSWORD = "MAILGUN_SMTP_PASSWORD";
	
	protected $hasCredentials = false;
	
	public function hasCredrentials() { return $this->hasCredentials; }
	
	public function getServer() { return $this->get(self::KEY_SERVER); }
	public function getPort() { return $this->get(self::KEY_PORT); }
	public function getApiKey() { return $this->get(self::KEY_APIKEY); }
	public function getUsername() { return $this->get(self::KEY_USERNAME); }
	public function getPassword() { return $this->get(self::KEY_PASSWORD); }
	
	public function __construct()
	{
		parent::__construct();
		
		$cctrlConfig = \BF\Config\CloudControl\Configuration::getInstance();
		$mailgun = $cctrlConfig->get("MAILGUN");
		if (is_array($mailgun) && count($mailgun)) {		
			$this->hasCredentials = true;
			$this->mergeArray($mailgun);				
		}
	}
}