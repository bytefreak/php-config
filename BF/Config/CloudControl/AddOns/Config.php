<?php
namespace BF\Config\CloudControl\AddOns;

class Config extends \BF\Config\JSONConfiguration
{
	public function __construct()
	{
		parent::__construct();
		$cctrlConfig = \BF\Config\CloudControl\Configuration::getInstance();
		$config = $cctrlConfig->get("CONFIG.CONFIG_VARS",array());
		$this->mergeArray($config);
	}
}