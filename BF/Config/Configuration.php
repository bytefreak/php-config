<?php
namespace BF\Config;

/**
 * Class Configuration
 *
 * @package BF\Config
 */
class Configuration
{
    /**
     * the configuration data
     * @var array
     */
    protected $data;

    /**
     * get an item using a keypath
     *
     * @param string $keypath
     * @param mixed $default
     * @see array_get_by_keypath
     * @return mixed
     */
    public function get($keypath,$default=null) { return array_get_by_keypath($this->data, $keypath, $default); }

    /**
     * sets an item using a keypath
     *
     * @param string $keypath
     * @param mixed $value
     */
    public function set($keypath,$value) { array_set_by_keypath($this->data, $keypath, $value); }

    /**
     * created the empty configuration container
     */
    public function __construct() { $this->data  = array(); }

    /**
     * merges all entries in this configuration container
     *
     * @param array $array
     */
    public function mergeArray($array)
	{
		$this->data = array_merge_recursive_simple($this->data,$array);
	}

    /**
     * merges an other configuration container in this one.
     *
     * @param Configuration $config
     */
    public function mergeConfiguration(Configuration $config)
	{
		$this->mergeArray($config->data);
	}

    /**
     * interprets all item-keys in this configurationcontainer as keypaths and expands them
     *
     * @param array $node
     * @return array
     */
    protected function expandShorts($node)
	{
		if (!is_array($node)) return $node;
	
		$newNode = array();
		foreach ($node as $path => $value) {
			$value = $this->expandShorts($value);
			array_set_by_keypath($newNode, $path, $value);
		}
	
		return $newNode;
	}	
}