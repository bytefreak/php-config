<?php
/**
 * @author thomas
 * @date 06.08.14-15:11
 */

namespace BF\Config\Tests;

use BF\Config\JSONConfiguration;

class JSONConfigurationJSONRefTest extends \PHPUnit_Framework_TestCase
{


    public function testResolvSimpleInline()
    {
        $c = new JSONConfiguration(true);

        $c->mergeArray(array(
           'itemA' => 'valueA',
           'itemB' => array('$ref' => "#/itemA")
        ));

        $this->assertEquals(array('$ref' => "#/itemA"),$c->get("itemB"));

        $c->resolveReferences();

        $this->assertEquals('valueA',$c->get("itemB"));
    }

    public function testResolvNotFound()
    {
        $c = new JSONConfiguration(true);

        $c->mergeArray(array(
            'itemA' => 'valueA',
            'itemB' => array('$ref' => "#/itemC")
        ));

        $this->setExpectedException('\Exception');
        $c->resolveReferences();

        $this->assertEquals('valueA',$c->get("itemB"));
    }

    public function testOnlyLocalReference()
    {
        $this->setExpectedException('\Exception');
        $c = new JSONConfiguration(true);

        $c->mergeArray(array(
            'itemA' => 'valueA',
            'itemB' => array('$ref' => "kljaslds://")
        ));
        $c->resolveReferences();
    }
}
 