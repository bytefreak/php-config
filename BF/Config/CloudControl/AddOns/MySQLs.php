<?php
namespace BF\Config\CloudControl\AddOns;

class MySQLs extends \BF\Config\Configuration
{
	const KEY_HOST = "MYSQLS_HOSTNAME";
	const KEY_DATABASE = "MYSQLS_DATABASE";
	const KEY_USERNAME = "MYSQLS_USERNAME";
	const KEY_PASSWORD = "MYSQLS_PASSWORD";
	const KEY_PORT = "MYSQLS_PORT";
	
	protected $hasCredentials = false;
	
	public function hasCredrentials() { return $this->hasCredentials; }
	public function getUsername() { return $this->get(self::KEY_USERNAME); }
	public function getPassword() { return $this->get(self::KEY_PASSWORD); }
	public function getHostname() { return $this->get(self::KEY_HOST); }
	public function getDatabase() { return $this->get(self::KEY_DATABASE); }
	public function getPort() { return $this->get(self::KEY_PORT); }
	
	public function __construct()
	{
		parent::__construct();
		
		$cctrlConfig = \BF\Config\CloudControl\Configuration::getInstance();
		$mysqls = $cctrlConfig->get("MYSQLS");
		if (is_array($mysqls) && count($mysqls)) {		
			$this->hasCredentials = true;
			$this->mergeArray($mysqls);				
		}
	}
}