<?php
/**
 * @author thomas
 * @date 08.08.14-18:35
 */

namespace BF\Config\Tests;


use BF\Config\JSONConfiguration;

class JSONConfigurationTest extends \PHPUnit_Framework_TestCase
{

    public function testInstance()
    {
        $c = new JSONConfiguration();
        $this->assertInstanceOf('\BF\Config\Configuration',$c);
    }

    public function testNotJson()
    {
        $this->setExpectedException('\Exception');
        $c = new JSONConfiguration();
        $c->addJSONFile(__FILE__);
    }



    public function testJsonFileCallsString()
    {
        $c = $this->getMock('\BF\Config\JSONConfiguration',array('addJSONString'));
        $c->expects($this->once())->method('addJSONString');
        $c->addJSONFile(__FILE__);
    }

    public function testFileNotFound()
    {
        $this->setExpectedException('\Exception');
        $c = new JSONConfiguration();
        $c->addJSONFile('/'.md5(uniqid()));
    }

    public function testExtends()
    {
        $c = $this->getMock('\BF\Config\JSONConfiguration',array('addJSONFile'));
        $c->expects($this->once())->method('addJSONFile')->with(getcwd()."/other.json");
        $c->addJSONString('{"extends":"other.json","config" : { "a":1 }}',getcwd());
    }

    public function testConfigNeeded()
    {
        $c = $this->getMock('\BF\Config\JSONConfiguration',array('addJSONFile'));
        $c->expects($this->once())->method('addJSONFile');
        $this->setExpectedException('\Exception');
        $c->addJSONString(json_encode(array(
            "extends" => "xxx.json"
        )));
    }

    public function testWithConfig()
    {
        $c = new JSONConfiguration();
        $c->addJSONString(json_encode(array("config" => array("itemA" => "valueA"))));
        $this->assertEquals("valueA",$c->get("itemA"));

    }

    public function testWithWithoutConfig()
    {
        $c = new JSONConfiguration();
        $c->addJSONString(json_encode(array("itemA" => "valueA")));
        $this->assertEquals("valueA",$c->get("itemA"));

    }

}
 