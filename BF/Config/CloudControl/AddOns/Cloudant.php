<?php
namespace BF\Config\CloudControl\AddOns;

class Cloudant extends \BF\Config\Configuration
{
	const KEY_CLOUDANT_URL = "CLOUDANT_URL";
	const KEY_CLOUDANT_USERNAME = "CLOUDANT_USERNAME";
	const KEY_CLOUDANT_PASSWORD = "CLOUDANT_PASSWORD";
	
	protected $hasCredentials = false;
	
	public function hasCredrentials() { return $this->hasCredentials; }
	public function getUsername() { return $this->get(self::KEY_CLOUDANT_USERNAME); }
	public function getPassword() { return $this->get(self::KEY_CLOUDANT_PASSWORD); }
	public function getURL() { return $this->get(self::KEY_CLOUDANT_URL); }
	
	public function __construct()
	{
		parent::__construct();
		
		$cctrlConfig = \BF\Config\CloudControl\Configuration::getInstance();
		$url = $cctrlConfig->get("CLOUDANT.CLOUDANT_URL");
		if ($url) {		
			$this->hasCredentials = true;
		}
		$cuser = parse_url($url,PHP_URL_USER);
		$cpass = parse_url($url,PHP_URL_PASS);
		
		$this->mergeArray(array(
			self::KEY_CLOUDANT_URL => $url,
			self::KEY_CLOUDANT_USERNAME => $cuser,
			self::KEY_CLOUDANT_PASSWORD => $cpass
		));				
	}
}