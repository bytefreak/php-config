<?php
namespace BF\Config;

/**
 * Class JSONConfiguration
 * @package BF\Config
 */
class JSONConfiguration extends Configuration
{
    /**
     * @inheritdoc
     */
    public function __construct()
	{
		parent::__construct();
	}

    /**
     * adds a json file
     *
     * @param string $jsonFile
     * @throws \Exception
     */
    public function addJSONFile($jsonFile)
	{
		$jsonFile = \BF\PhpUtils\FileSystem::cleanPath($jsonFile);

		if (!file_exists($jsonFile)) throw new \Exception("File $jsonFile not found!");

        $this->addJSONString(file_get_contents($jsonFile),dirname($jsonFile));
    }

    /**
     * adds a json string
     *
     * @param string $jsonString
     * @param string $baseDir
     * @throws \Exception
     */
    public function addJSONString($jsonString,$baseDir=null)
    {
        $data = json_decode($jsonString,true);
		if (!$data) {
			switch (json_last_error()) {
				case JSON_ERROR_UTF8: throw new \Exception("JSON not UTF-8 encoded!");
				case JSON_ERROR_CTRL_CHAR: throw new \Exception("JSON control char error. Wrong encoding?");
				case JSON_ERROR_DEPTH: throw new \Exception("JSON max depth reached?");
				case JSON_ERROR_STATE_MISMATCH: throw new \Exception("JSON state mismatch?");
				case JSON_ERROR_SYNTAX: throw new \Exception("JSON syntax error?");
			}
		}

		if (isset($data["extends"])) {
			$extendedFile = $data["extends"];
			if (!\BF\PhpUtils\FileSystem::isAbsolute($extendedFile)) {
                if (is_null($baseDir)) $baseDir = getcwd();
                $extendedFile = $baseDir."/".$extendedFile;
            }
			$this->addJSONFile($extendedFile);
		}

		if (!isset($data["config"]) || !is_array($data["config"])) {
            if (isset($data["extends"])) {
                throw new \Exception("no 'config' top element found in json config");
            }else{
                $data = array("config" => $data);
            }
        }
		$config = $data["config"];

		$config = $this->expandShorts($config);

		$this->mergeArray($config);
	}

    /**
     * resolves all '$ref' nodes in json reference styles
     */
    public function resolveReferences()
    {
        $this->data = $this->_resolveRef($this->data);
    }

    /**
     * recursive function to resolv json reference nodes
     *
     * @param mixed $node
     * @param string $path
     * @return mixed
     */
    protected function _resolveRef($node,$path="")
    {
        if (!is_array($node)) return $node;

        if (isset($node['$ref'])) {
            return $this->_findNodeWithPath($node['$ref']);
        }

        foreach ($node as $k => $v) {
            $node[$k] = $this->_resolveRef($v,$path."/".$k);
        }

        return $node;
    }

    /**
     * finds a node references with a json reference path
     *
     * @param string $path
     * @return mixed
     * @throws \Exception
     */
    protected function _findNodeWithPath($path)
    {
        $originalPath = $path;

        $sourceData = $this->_findSourceDataFromPath($path);

        $tag = uniqid();

        $v = array_get_by_keypath($sourceData,str_replace('/','.',trim($path,'/')),$tag);
        if ($v==$tag) throw new \Exception("Reference $originalPath not found!");

        return $v;
    }

    /**
     * extracts and returns the source configuration data for the provided json reference path
     * the path will be transformed to be local in this container
     *
     * @param string $path
     * @return array
     * @throws \Exception
     */
    protected function _findSourceDataFromPath(&$path)
    {
        if ($path[0]=="#") {
            $path = substr($path,1);
            return $this->data;
        }
        throw new \Exception("Currently only local file is supported for references! (using #<PATH>)");
    }

}