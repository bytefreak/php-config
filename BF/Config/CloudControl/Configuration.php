<?php
namespace BF\Config\CloudControl;

class Configuration extends \BF\Config\JSONConfiguration
{
	public function __construct()
	{
		parent::__construct();
		
		$credFile = isset($_ENV['CRED_FILE']) ? $_ENV['CRED_FILE'] : getenv("CRED_FILE"); 
		if ($credFile && file_exists($credFile)) {
			$data = json_decode(file_get_contents($credFile),true);
			if (!$data) throw new \Exception("$credFile contains an error!");
			$data = $this->expandShorts($data);
			$data["_loadedFiles"] = array($credFile => true);
			$this->mergeArray($data);
		}		
	}
	
	public static function getInstance()
	{
		static $config = null;
		if (is_null($config)) $config = new self();
		return $config;
	}
}